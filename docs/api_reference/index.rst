***************************
API reference
***************************

DataFrame & Series related functions
====================================

.. autosummary::
   :toctree: both

   handadoc_client
