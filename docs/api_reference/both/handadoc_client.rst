﻿handadoc\_client
================

.. automodule:: handadoc_client

   
   
   .. rubric:: Module Attributes

   .. autosummary::
   
      DOCUMENTATION_NAME_PATTERN
      SPHINX_SETUP_FILENAME
      PROJECT_SETUP_FILENAME
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_docu_sub_path
      get_target_path_of_documentation
      is_static_html_file
      unzip_documentation
      validate_docu_setup
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      DocuSetup
      ProcessMessage
   
   

   
   
   



