# Handadoc Client 
[![Coverage Status](https://coveralls.io/repos/gitlab/david.scheliga/handadoc_client/badge.svg?branch=release)](https://coveralls.io/gitlab/david.scheliga/handadoc_client?branch=release)
[![Build Status](https://travis-ci.com/david.scheliga/handadoc_client.svg?branch=release)](https://travis-ci.com/david.scheliga/handadoc_client)
[![PyPi](https://img.shields.io/pypi/v/handadoc_client.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/handadoc_client/)
[![Python Versions](https://img.shields.io/pypi/pyversions/handadoc_client.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/handadoc_client/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

**handadoc_client** is a command line tool for uploading build sphinx html pages to
a handadoc webserver.

![handadoc_client-icon](https://handadoc_client.readthedocs.io/en/latest/_images/handadoc_client-icon.svg)

## Installation

```` shell script
    $ pip install handadoc-client
````

## Alpha Development Status

The current development state of this project is *alpha*. Towards the beta

- naming of modules, classes and methods will change, since the final wording is not
  done.
- Code inspections are not finished.
- The documentation is broad or incomplete.
- Testing is not complete, as it is added during the first test phase. At this
  state mostly doctests are applied at class or function level.


## Basic Usage

[Read-the-docs](https://handadoc_client.readthedocs.io/en/latest/index.html) for a more detailed documentation.

## Contribution

Any contribution by reporting a bug or desired changes are welcomed. The preferred 
way is to create an issue on the gitlab's project page, to keep track of everything 
regarding this project.

### Contribution of Source Code
#### Code style
This project follows the recommendations of [PEP8](https://www.python.org/dev/peps/pep-0008/).
The project is using [black](https://github.com/psf/black) as the code formatter.

#### Workflow

1. Fork the project on Gitlab.
2. Commit changes to your own branch.
3. Submit a **pull request** from your fork's branch to our branch *'dev'*.

## Authors

* **David Scheliga** 
    [@gitlab](https://gitlab.com/david.scheliga)
    [@Linkedin](https://www.linkedin.com/in/david-scheliga-576984171/)
    - Initial work
    - Maintainer

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the
[LICENSE](LICENSE) file for details

## Acknowledge

[Code style: black](https://github.com/psf/black)
